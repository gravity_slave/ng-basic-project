import { BasicNgPage } from './app.po';

describe('basic-ng App', () => {
  let page: BasicNgPage;

  beforeEach(() => {
    page = new BasicNgPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
