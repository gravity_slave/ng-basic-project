export class Recipe {
  public name: string;
  public  description: string;
  public imagePath: string;

  constructor(name: string, descripton: string, imagePAth: string) {
    this.name = name;
    this.description = descripton;
    this.imagePath = imagePAth;
  }
}
