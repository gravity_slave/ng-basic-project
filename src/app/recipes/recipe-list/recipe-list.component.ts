import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Array<Recipe>
  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  constructor() {
    this.recipes = [
    new Recipe('A test recipe', 'This is simply a test',
    'http://4.bp.blogspot.com/-BRjaz-haFww/UOJB7eOkChI/AAAAAAAAAsw/PScenymIazk/s1600/download+(4).jpg')
    ]
  }
  ngOnInit() {
  }
  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }
}
